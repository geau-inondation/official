# Découpage d'un rapport

Pour découper un rapport en mettant les fichiers de section dans un autre dossier que le fichier principal, si ces fichiers font appel à des images appelées en chemin relatif (ce qui est mieux), il faut utiliser la commande suivante :

```{r eval = FALSE}
rmarkdown::render("rapport.rmd",
    knit_root_dir = getwd(),
    intermediates_dir = getwd())
```

Cette commande a des paramètres supplémentaires qui ne sont pas remplis par défaut dans l'appel réalisé dans VSCode avec le bouton, du coup on ne peut pas l'utiliser sans erreur !

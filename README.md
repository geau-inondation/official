```bash
:~$ cat ./routine
Eat → sleep → code → sport → repeat :p
```

# General R coding good practices and conventions for package development in the *Equipe inondation*

### Tools

- Package *devtools* is powerful. Learn it and use it 
- Package *usethis* is powerful. Learn it and use it 

### Dependencies

- Avoid as much as possible the use of functions not included in base R. This way we will reduce the number of dependencies of the package and the chance that it does not work because a library is discontinued.
- Dependencies should be listed in the DESCRIPTION file.
- When writing a function, avoid using *@import* and/or *@ImportFrom*. The package should be listed in DESCRIPTION and the function should be called using the notation *package::function()*. This helps maintenance and code readability

### Style

- Avoid long if statements as much as you can. Perhaps you should do function 
- Avoid using *'$'* to select variables in a data.frame or in a list. Please use the form `mydataframe["variable"]`.
- Avoid using `getwd()` to get the absolute path of a file. Instead use the `normalizePath()` function include in base R.
- Avoid using `setwd()`in your scripts. Your folder tree is not my folder tree  
- Avoid using `<<-`. There exists other ways to the same that are clearer and less prone to errors.


### Repeating structures and iterating 

- If you are going to repeat the same structure twice or more, think about doing a proper function or stocking the result in a constant that can be invoked anywhere in the code. This will make your code less prone to errors if modifications should be made.
- Similarly, before making a function, search or ask other maintainers if a function is already made. This make you save time, help maintain the consistency and coherency of the package. It will also help maintenance. For instance, a function to select elements of interest according to certain criterion might already be implemented as internal function to the library. If the way of selecting elements should change, it will be better to have used such a function in your code so the modifications done to select elements are shown coherently throughout functions.    

### vignettes 

- Preferably, create new vignettes using the function `usethis::use_vignette("name-of-the-new-vignette")` instead of copy-pasting existing ones. This function will create a Rmd file with the bare minimum information and in the right folder.

## Specific for floodam.data

- When introducing new dataset to process a database (e.g. the *BD Topo*), the dataset should start with a prefix that allows us to track its origin. For example, let us assume that we want to introduce a new dataset that is going to be used while processing the database *BD-Topo*. Let us assume that such a dataset is going to substitute the codes used to codify the material used to build the roof by the actual name of the material. In this case this dataset should be named **bd_topo_roof_material**
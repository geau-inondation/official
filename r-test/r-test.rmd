---
title: "Quelques test liés à R"
subtitle: "Pour aider à mieux coder"
author:
    - name: Grelot Frédéric
      affiliation: G-eau, INRAE
date: "`r `"
output:
    bookdown::pdf_book:
        toc: true                       # table des matières
        toc_depth: 4                    # profondeur de la table des matières
        toc_unnumbered: true            # sections non numérotées dans la table des matières
        toc_appendix: true              # annexe dans la table des matières
        toc_bib: true                   # bibliographie dans la table des matières
        number_sections: true           # numérotation des sections
        fig_caption: true               # 
        citation_package: natbib        # gestion de la bibliographie
        extra_dependencies: 
            booktabs: null              # jolis tableaux
            float: null                 # gestion des flottants
        includes:
            before_body: "data-common/layout/preamble.tex"  
lang: fr
bibliography: "data-common/bibliographie/.bibliographie-commune.bib"
---

\newpage

\listoffigures

\listoftables

\newpage

```{r setup, echo = FALSE}
knitr::opts_chunk$set(
    warning = FALSE,
    message = FALSE,
    fig.align = "center"
)
```

```{r child="data-structure.rmd"}
```

# (APPENDIX) Appendix {-} 

